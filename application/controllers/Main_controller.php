<?php 
require(APPPATH . 'libraries/REST_Controller.php');
class Main_controller extends REST_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Main_model');
    }
    // Insert Country
    function index_post() {
        $data = array(
            'name' => $this->input->post('name'),
            'country_id' => $this->input->post('country_id'),
            'state_id' => $this->input->post('state_id')
        );

        $query = $this->db->insert('users', $data);

        if($query == 1) {
            $this->response(['Country inserted successfully.'], REST_Controller::HTTP_OK);
        } else {
            echo "Something went wrong while inserting data !!";
        }
    }
    // Insert State
    function state_post() {
        $data = array(
            'name' => $this->input->post('name'),
            'country_id' => $this->input->post('country_id'),
            'state_id' => $this->input->post('state_id')
        );

        $query = $this->db->insert('users', $data);

        if($query == 1) {
            $this->response(['State inserted successfully.'], REST_Controller::HTTP_OK);
        } else {
            echo "Something went wrong while inserting data !!";
        }
    }
    // Insert City
    function city_post() {
        $data = array(
            'name' => $this->input->post('name'),
            'country_id' => $this->input->post('country_id'),
            'state_id' => $this->input->post('state_id')
        );

        $query = $this->db->insert('users', $data);

        if($query == 1) {
            $this->response(['City inserted successfully.'], REST_Controller::HTTP_OK);
        } else {
            echo "Something went wrong while inserting data !!";
        }
    }
    // Fetch State
    function fetch_state_post() {
        if ($this->input->post('country_id')) {
            echo $this->Main_model->fetch_state($this->input->post('country_id'));
        }
    }
    // Country already exist
    function country_not_registered_post() {
        if ($this->Main_model->name_exists($this->input->post('name')) == TRUE) {
            echo json_encode(FALSE);
        } else {
            echo json_encode(TRUE);
        }
    }
}
?>