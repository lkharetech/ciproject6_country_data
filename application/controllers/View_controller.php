<?php
class View_controller extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('Main_model');
    }
    function index() {

        $data['country'] = $this->Main_model->fetch_country();

        $this->load->view('main_view',$data);
    }
}
