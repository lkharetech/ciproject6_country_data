<?php 
class Main_model extends CI_Model {
    // View Country 
    function fetch_country() {
        $this->db->order_by("name", "ASC");
        $this->db->where ('country_id', 0);
        $query = $this->db->get("users");
        return $query->result_array();  
    }
    // View State
    function fetch_state($country_id) {
        $this->db->where('country_id', $country_id);
        $this->db->where('state_id', 0);
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get('users');
        $output = '<option value="">Select State</option>';
        foreach ($query->result() as $row) {
            $output .= '<option value="' . $row->id . '">' . $row->name . '</option>';
        }
        return $output;
    }
    // Check whether name exists
    function name_exists($name) {
        $this->db->where('name', $name);
        $query = $this->db->get('users');
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
?>