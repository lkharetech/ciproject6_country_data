<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>

    <title>Document</title>
</head>

<body>
    <div class="container">
        <hr>
        <h3 style="text-align: center;">MAIN PAGE RELATIONAL DATABASE PROJECT</h3>
        <div>
            <p name="message" id="message" style="color: green;"></p>
        </div>
        <hr>
        <div>
            <h5>Enter Country Data</h5>
            <form id="countryform">
                <label for="">Country Name : </label>
                <input type="text" name="name" id="country" placeholder="Enter country name"><br>
                <input type="hidden" name="country_id" value="0">
                <input type="hidden" name="state_id" value="0">
                <input type="submit" name="submit" id="countrysubmit" value="ADD">
            </form>
        </div>
        <hr>
        <div>
            <h5>Enter State Data</h5>
            <form id="stateform">
                <label for="">Choose Country : </label>
                <select name="country_id">
                    <option value="">--Select Country--</option>
                    <?php
                    foreach ($country as $row) {
                        echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
                    }
                    ?>
                </select><br><br>
                <input type="hidden" name="state_id" value="0">
                <label for="">State Name : </label>
                <input type="text" name="name" id="state" placeholder="Enter state name"><br>
                <input type="submit" name="submit" id="statesubmit" value="ADD">
            </form>
        </div>
        <hr>
        <div>
            <h5>Enter City Data</h5>
            <form id="cityform">
                <label for="">Choose Country : </label>
                <select name="country_id" id="country_id">
                    <option value="">--Select Country--</option>
                    <?php
                    foreach ($country as $row) {
                        echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
                    }
                    ?>
                </select><br><br>
                <label for="">Choose State : </label>
                <select name="state_id" id="state_id">
                    <option value="">--Select State--</option>
                </select><br><br>
                <label for="">City Name : </label>
                <input type="text" name="name" id="city" placeholder="Enter city name"><br>
                <input type="submit" name="submit" id="citysubmit" value="ADD">
            </form>
        </div>
        <hr>

    </div>

</body>

</html>

<script>
    // Validate Country
    $("#countryform").validate({
        rules: {
            name: {
                required: true,
                remote: {
                    url: "main_controller/country_not_registered",
                    type: "post",
                    data: {
                        name: function () { return $("#country").val(); }
                    },
                }
            }
        },
        messages: {
            name: {
                required: "Country can't be blank !!",
                remote: 'Country already exists !!',
            },
        },
        // Add country function
        submitHandler: function(form) {
            $.ajax({
                url: "main_controller",
                type: "POST",
                dataType: "JSON",
                processData: false,
                contentType: false,
                data: new FormData($('#countryform')[0]),
                success: function(response) {
                    console.log(response);
                    $("#message").text("Country Successfully Inserted !!");
                    document.getElementById("countryform").reset();
                }
            });
        }
    });
    // Validate country & state
    $("#stateform").validate({
        rules: {
            name: {
                required: true,
                remote: {
                    url: "main_controller/country_not_registered",
                    type: "post",
                    data: {
                        name: function () { return $("#state").val(); }
                    },
                }
            },
            country_id: {
                required: true,
            }
        },
        messages: {
            name: {
                required: "State can't be blank !!",
                remote: 'State already exists !!',
            },
            country_id: {
                required: "Country can't be blank !!",
            }
        },
        // Add state function
        submitHandler: function(form) {
            $.ajax({
                url: "main_controller/state",
                type: "POST",
                dataType: "JSON",
                processData: false,
                contentType: false,
                data: new FormData($('#stateform')[0]),
                success: function(response) {
                    console.log(response);
                    $("#message").text("State Successfully Inserted !!");
                    document.getElementById("stateform").reset();
                }
            });
        }
    });
    // Validate country, state & city
    $("#cityform").validate({
        rules: {
            name: {
                required: true,
                remote: {
                    url: "main_controller/country_not_registered",
                    type: "post",
                    data: {
                        name: function () { return $("#city").val(); }
                    },
                }
            },
            country_id: {
                required: true,
            },
            state_id: {
                required: true,
            }
        },
        messages: {
            name: {
                required: "City can't be blank !!",
                remote: 'City already exists !!',
            },
            country_id: {
                required: "Country can't be blank !!",
            },
            state_id: {
                required: "State can't be blank !!",
            }
        },
        // Add city function
        submitHandler: function(form) {
            $.ajax({
                url: "main_controller/city",
                type: "POST",
                dataType: "JSON",
                processData: false,
                contentType: false,
                data: new FormData($('#cityform')[0]),
                success: function(response) {
                    console.log(response);
                    $("#message").text("City Successfully Inserted !!");
                    document.getElementById("cityform").reset();
                }
            });
        }
    });
    $(document).ready(function() {
    // Fetch state
    $('#country_id').change(function() {
        var country_id = $('#country_id').val();
        if (country_id != '') {
            $.ajax({
                url: "main_controller/fetch_state",
                method: "POST",
                data: {
                    country_id: country_id
                },
                success: function(data) {
                    $('#state_id').html(data);
                }
            });
        } else {
            $('#state_id').html('<option value="">Select State</option>');
        }
    });
});
</script>